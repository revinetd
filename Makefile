CC = gcc
CFLAGS = -Wall -g -O2  -DHAVE_CONFIG_H
LDFLAGS =  
OBJS = getopt.o revinetd.o misc.o proxy.o server.o relay_agt.o statistics.o
BINDIR = /usr/local/bin
INSTALL = ./install-sh

all : revinetd

revinetd : 	$(OBJS)
		$(CC) -o $@ $(OBJS) $(LDFLAGS)

clean :
	-rm -f core *.o revinetd

distclean :
	-rm -f core *.o config.h revinetd Makefile config.status config.log

install:
	$(INSTALL) -o 0 -g 0 -m 755 -c -s revinetd $(BINDIR)
