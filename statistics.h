#pragma once


struct Statistics_str {
    unsigned int    nmb_client_accepts_acc;
    unsigned int    nmb_sock_queue_enqueues_acc;
    unsigned int    size_sock_queue;
    unsigned int    nmb_relay_worker_conns_acc;
    unsigned int    nmb_relay_worker_conns_cur;
    unsigned int    nmb_keep_alives_recv;
};
typedef struct Statistics_str Statistics;

void reset_stats(Statistics *st);

