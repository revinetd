#include "string.h"

#include "statistics.h"

Statistics statistics;

void reset_stats(Statistics *st) 
{
    st->nmb_client_accepts_acc = (0);
    st->nmb_sock_queue_enqueues_acc = (0);
    st->size_sock_queue = (0);
    st->nmb_relay_worker_conns_acc = (0);
    st->nmb_relay_worker_conns_cur = (0);
    st->nmb_keep_alives_recv = (0);
}

