/*
 * server.c
 *
 * This file is a part of the revinetd project              
 *
 * Revinetd is copyright (c) 2003-2008 by Steven M. Gill
 * and distributed under the GPL.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 *                                                                        */

/*
    Changelog: 

        1.0.2_p6    - adding logging of foreign address

        p5          - option switched statistics output
                    - statistics.size_sock_queue now also used for controlling
                      priority of handling (##004)

        p4          - adding "continue"s in the main loop for proxy_sock acceptance
                      and ra_sock acceptance (##001 and ##002)
                    - introducing auto sockq cleanup (by re-requesting a worker connection
                      if necessary) (##003)
                      For this, timeout to the select() call had to be added, and
                      "create_time" member introduced.
                      Avoided CPU overload by a usleep(10000) call near "rc == 0".
                    - prepared: outputting connection statistics
        
        2020-06-09  - complete restructuring of the main loop, to obtain
        (thp)         correct behaviour for relay agent connection loss and
                      reconnection. 
                      Now, the proxy_sock accept branch is prevented,
                      thus avoiding blocking or idling.
                      [Could optionally shutdown client-facing listen socket
                      upon relay agent's disconnect; not implemented]
                    - slight adjustment of error/info messages
                    - code line rearrangement near "chan_remove(temp)"
                    - verbosity handling adjusted
 */
 
#include "revinetd.h"
#include "includes.h"
#include "proxy.h"
#include "server.h"
#include "misc.h"
#include "statistics.h"

#include "time.h"

extern Conf conf;
extern Channels *chan;

static const char cvsid[] = "$Id: server.c,v 1.28 2008/08/28 03:24:59 necrotaur Exp $";

extern Statistics statistics;
time_t next_display_time = 0;
#define STATS_INTVL         3

// On the client-facing end:
#define LISTEN_QUEUE_SIZE   10

void
cond_print_stats() 
{
    time_t now = time(NULL);
    if (now >= next_display_time)
    {
        printf("%8d\t%8d\t%8d\t%8d\t%8d\t%8d\t%8d\n", 
                statistics.nmb_client_accepts_acc,
                statistics.nmb_sock_queue_enqueues_acc,
                statistics.size_sock_queue,
                statistics.nmb_relay_worker_conns_acc,
                statistics.nmb_relay_worker_conns_acc - statistics.nmb_relay_worker_conns_cur,
                statistics.nmb_relay_worker_conns_cur,
                statistics.nmb_keep_alives_recv);
        fflush(stdout);
        next_display_time = now + STATS_INTVL;
    }
}

// Allows "host" to be of format "[xxxx:xxxx:...:xxxx]".
int
server(char *host,int port, char *host2, int port2)
{
    int ra_sock, proxy_sock, tmp_sock, tmp_sock_4or6, bh_sock = -1;
    fd_set active, read;
    struct sockaddr_in tmp_sin;
    struct sockaddr_in6 tmp_sin6;
    Channels *tmp_chan, *temp;
    SockQueue *tmp_sq, *sockq = NULL;
    socklen_t size;

    struct timeval timeout; // select() timeout spec
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    /* Make the sockets. */
    char host_strp[strlen(host)];
    int proxy_af = detect_address_family(host, host_strp);
    if (proxy_af < 0) {
        fprintf(stderr, "server(): listen-client hostname value is not in valid format: %s\n", host);
        exit(EXIT_FAILURE);
    }

    if (conf.verbosity >= VB_NORMAL) printf("I: Binding relay end server socket...\n");
    ra_sock = make_socket_IPv4(host2, port2);
    if (conf.verbosity >= VB_NORMAL) printf("I: Binding client end server socket...\n");
    proxy_sock = (proxy_af == AF_INET6 ? make_socket_IPv6(host_strp, port) : make_socket_IPv4(host, port));
    register_sock(ra_sock);
    register_sock(proxy_sock);
    
    if (listen(ra_sock, 1) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    if (conf.verbosity >= VB_NORMAL)
        printf("I: Waiting for relay agent on %s:%u\n", host2, port2);

    FD_ZERO(&active);
    FD_SET(ra_sock, &active);

    // Now start to listen (pro-forma) on the client-facing end:
    if (listen(proxy_sock, LISTEN_QUEUE_SIZE) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    // ... but do not actually accept while ra not yet connected:
    //No: FD_SET(proxy_sock, &active);

    /* No channels yet. */
    chan = NULL;

    reset_stats(&statistics);
   
    /* Start the access logging: */ 
    if (strlen(conf.access_log_filepath) > 0) {
        conf.access_log_file = fopen(conf.access_log_filepath, "a");
        if (conf.access_log_file == NULL) {
            fprintf(stderr, "Unable to open access log file for writing.\n");
            exit(-1);
        }
    }

    while (1) {
        if (conf.verbosity >= VB_DEBUG)
            cond_print_stats();

        if (sockq != NULL) { // ##003
            if (sockq->create_time + 4 < time(NULL)) // if the sockq entry is already 4 secs old, regenerate a worker connection request:
            {
                if (conf.verbosity >= VB_VERBOSE)
                    printf("I: Extraordinarily requesting new proxy pair from relay agent\n");
                send_comm_message(bh_sock, RA_TARGET_UP);
                sockq->create_time = time(NULL);
            }
        }

        read = active;
        int rc = select(FD_SETSIZE, &read, NULL, NULL, &timeout);
        if (rc < 0) {
            perror("select");
            exit(EXIT_FAILURE);
        }
        if (rc == 0) {
            usleep(10000); /* wait for 10 msecs */
            continue;
        }
        
        // Handle changes on the relay side with priority:
        if (FD_ISSET(ra_sock, &read)) {
            /* Connect with relay agent. */
            size = sizeof(tmp_sin);
            tmp_sock = accept(ra_sock, (struct sockaddr *)&tmp_sin, &size);
            if (tmp_sock < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            register_sock(tmp_sock);

            if (bh_sock == -1) {
                if (conf.verbosity >= VB_NORMAL)
                    printf("I: Accepting relay agent from %s:%u\n", 
                            inet_ntoa(tmp_sin.sin_addr),
                            ntohs(tmp_sin.sin_port));
                bh_sock = tmp_sock;
                FD_SET(bh_sock, &active);
                // Switch on accepting client connections later:
                FD_SET(proxy_sock, &active);
                listen(proxy_sock, LISTEN_QUEUE_SIZE);
                if (conf.verbosity >= VB_VERBOSE)
                    printf("I: (Re-)Activating waiting for clients on %s:%u\n", host, port);
            } else {
                if (sockq == NULL) {
                    if (conf.verbosity >= VB_NORMAL)
                        printf("W: Unrequested connection from another relay agent.\n");
                    // TODO: closing tmp_sock here?
                } else {
                    // Any connection attempt that is made at the ra_sock
                    // end while the backhaul is standing is understood to
                    // be a data transfer channel established by the relay
                    // upon our own request. Thus, match it to the half-open
                    // connection queue pointed to by sockq:
                    
                    if (conf.verbosity >= VB_VERBOSE)
                        printf("I: New relay agent (worker) connection established from %s:%hu\n",
                            inet_ntoa(tmp_sin.sin_addr),
                            ntohs(tmp_sin.sin_port));

                    tmp_sq = sockq;
                 
                    if (chan == NULL) {
                        chan = chan_add();
                        tmp_chan = chan;
                    } else {
                        tmp_chan = chan;
                        while (tmp_chan->next != NULL)
                            tmp_chan = tmp_chan->next;
                        tmp_chan->next = chan_add();
                        tmp_chan->next->prev = tmp_chan;
                        tmp_chan = tmp_chan->next;
                    }

                    tmp_chan->source = tmp_sq->sock;
                    tmp_chan->target = tmp_sock;
                    tmp_chan->foreign_address_str = tmp_sq->foreign_address_str;
                    statistics.nmb_relay_worker_conns_acc++;
                    statistics.nmb_relay_worker_conns_cur++;

                    FD_SET(tmp_chan->source, &active);
                    FD_SET(tmp_chan->target, &active);

                    // Finally remove the processed from the queue of half-open 
                    // connections:
                    sockq = tmp_sq->next;
                    // (Since foreign_address_str value is carried over by pointer, do not 
                    // free. Rather, free in chan_remove().)
                    free(tmp_sq);
                    statistics.size_sock_queue--;
                }
            } /* end bh != -1 */ 
            continue;
                // ##001
        } /* end ra_sock readable */

        /*----------------------------------------------*/
                
        if (FD_ISSET(bh_sock, &read)) {
            /* We don't expect anything back. This should
               mean that comm is dropped.  relay agent is
               disconnected so reset state and wait for 
               another relay agent. */
            /* Slight amend, now we either get an error or a
             * heart beat... Hopefully this won't break anything. */
            if (get_comm_message(bh_sock) == SV_HEART_BEAT) {
                send_comm_message(bh_sock, RA_SERVER_ALIVE);
                statistics.nmb_keep_alives_recv++;
            } else {
                if (conf.verbosity >= VB_NORMAL)
                    printf("W: Relay agent disconnected.  Resetting...\n");
                unregister_sock(bh_sock);
                FD_CLR(bh_sock, &active);
                bh_sock = -1;
                temp = chan;
                while (temp != NULL) {
                    FD_CLR(temp->source, &active);
                    FD_CLR(temp->target, &active);
                    chan_remove(temp); // deallocates "temp" !!
                }
                // TODO:
                // Review: Could make sense to clear the sock queue here as well?
                // ...

                // Finally ensure that the proxy_sock accept() branch above 
                // is excluded from running, and avoid clients piling up in 
                // listen queue:
                FD_CLR(proxy_sock, &active);
                listen(proxy_sock, 0);
                if (conf.verbosity >= VB_NORMAL)
                    printf("W: Client-facing port stopping accepting connections.\n");
            }
        } /* end bh_sock ready */
        
        /*----------------------------------------------*/
        
        if (FD_ISSET(proxy_sock, &read)) {
            if (bh_sock == -1) {
                if (conf.verbosity >= VB_NORMAL)
                    printf("E: Ignoring a client-facing connection until relay agent is connected. (should not occur)\n");
                usleep(1000000); /* wait for a second */
                continue; /* Ignore until relay agent is connected. */
                // In fact, this branch should not occur anymore; the client-side
                // socket is only to be marked ready if the relay agent is available.
            }

            /* Accept the client, creating a client-side connection: */
            size = (proxy_af == AF_INET6 ? sizeof(tmp_sin6) : sizeof(tmp_sin));
            tmp_sock_4or6 = accept(proxy_sock, (struct sockaddr *)&tmp_sin6,
                    &size);
            if (tmp_sock_4or6 < 0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            register_sock(tmp_sock_4or6);
            statistics.nmb_client_accepts_acc++;

            char buf6[50];
            if ((conf.verbosity >= VB_VERBOSE) || (conf.access_log_file != NULL)) 
                inet_ntop(proxy_af, &(tmp_sin6.sin6_addr), buf6, 49);

            if (conf.verbosity >= VB_VERBOSE) {
                printf("I: New client connection detected from %s:%hu\n",
                        //inet_ntoa(tmp_sin6.sin6_addr),
                        buf6,
                        ntohs(tmp_sin6.sin6_port));
            }

            /* New sockq entry for half open connections. */
            if (sockq == NULL) {
                sockq = (SockQueue *)malloc(sizeof(SockQueue));
                if (sockq == NULL) {
                    perror("malloc");
                    exit(EXIT_FAILURE);
                }
                memset(sockq, 0, sizeof(SockQueue));
                tmp_sq = sockq;
            } else {
                tmp_sq = sockq;
                while (tmp_sq->next != NULL)
                    tmp_sq = tmp_sq->next;
                tmp_sq->next = (SockQueue *)malloc(sizeof(SockQueue));
                tmp_sq = tmp_sq->next;
                if (tmp_sq == NULL) {
                    perror("malloc");
                    exit(EXIT_FAILURE);
                }
                memset(tmp_sq, 0, sizeof(SockQueue));
            }
            tmp_sq->sock = tmp_sock_4or6;
            tmp_sq->create_time = time(NULL);
            tmp_sq->foreign_address_str = (conf.access_log_file != NULL ? strdup(buf6) : NULL);
            statistics.nmb_sock_queue_enqueues_acc++;
            statistics.size_sock_queue++;

            /* Now we send a request for a new backhaul channel. */
            if (conf.verbosity >= VB_VERBOSE)
                printf("I: Requesting new proxy pair from relay agent\n");
            send_comm_message(bh_sock, RA_TARGET_UP);

            if (statistics.size_sock_queue < 20)
                continue;
                // ##002: By adding "continue", client-side connection attempts 
                // are handled with priority, with effect that  
                // first all connections which are desired are accepted
                // and thus established, and only then afterwards the data
                // transfer begins. Therefore, do not fall through to proxy()
                // directly.
                // ##004: "if" conditioning switches mode as soon as load increases
        } /* end proxy_sock ready */

        /*----------------------------------------------*/
        
        // Process possible data which is ready on the worker
        // connections.
        proxy(&read, &active);
    }
    
    return(0);
}


